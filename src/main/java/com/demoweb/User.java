/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.demoweb;

import javax.inject.Named;
import javax.enterprise.context.RequestScoped;

/**
 *
 * @author kenwhiston
 */
@Named(value = "user")
@RequestScoped
public class User {
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    /**
     * Creates a new instance of User
     */
    public User() {
    }
    
}
